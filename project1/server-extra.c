#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>

#define PACKET_LANGE 512

//void clientProcess(int clientSocket);	// Server conneted to client, then process this function.

struct client_info
{
	int p1_passed;
	int length;
	int count;
	int even;
	int k;
	char lastword;
	char proto;
	char* send_buff;
};

int main(int argc, char *argv[])
{
	int serverSocket;
	int clientSocket;

	int client_addr_size;

	struct sockaddr_in server_addr;
	struct sockaddr_in client_addr;
	socklen_t len;

	fd_set active_set;
	fd_set read_set;
	//struct client_info* client_list = (struct client_info*)malloc (100);
	struct client_info client_list[100];
	int sock_max;

	//Argument Value
	int param_opt;
	int is_h = 0;
	char *port_arg = NULL;
	//End

	//Argument Parsing
	while((param_opt = getopt(argc, argv, "p:")) != -1)
	{
		switch(param_opt)
		{
			case 'p':
				is_h = 1;
				port_arg = optarg;
				printf( "port : %s\n", port_arg);
				break;
			case '?':
				printf("unknown option : %c\n", optopt);
				break;
		}
	}

	if (is_h == 0 || (port_arg == NULL))
	{
		printf( "Error : type -p port : %s\n", port_arg);
		exit(1);
	}
	//End

	FD_ZERO (&active_set);

	//Server addr memory set
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(atoi(port_arg));
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);

	//Make Socket
	if((serverSocket = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		printf("Error : Server can't init Socket\n");
		exit(1);
	}
	
	//Binding Socket
	if(bind(serverSocket, (struct sockaddr*)&server_addr, sizeof(server_addr)) == -1)	// assign address & port on 
	{
		printf("Error : Server can't bind Socket\n");
		exit(1);
	}

	if(listen(serverSocket, 1) < 0)
    {
        printf("Error : Server Can't listen.\n");
        exit(1);
    }

    FD_SET(serverSocket, &active_set);

    printf("Waiting Connection.\n");

	char* buff_rcv = (char *)malloc (10240);
	char* buff_sd = (char *)malloc (10240);
	bzero(buff_rcv, PACKET_LANGE+1);
	bzero(buff_sd, PACKET_LANGE+1);

	sock_max = serverSocket;



	while (1)
	{
		read_set = active_set;
		if (select (sock_max + 1, &read_set, NULL, NULL, NULL) == -1)
		{
			printf("Error : Select problem\n");
	       	exit(1);
		}
		int i;
		for (i = 0; i < sock_max + 1; ++i)
		{
			//printf("iii : %d %d\n", i, FD_ISSET (i, &read_set) );

			if(FD_ISSET (i, &read_set))
			{
				if (i == serverSocket)
				{
					len = sizeof(client_addr);
					clientSocket = accept (serverSocket, (struct sockaddr*)&client_addr, &len);
					if(clientSocket < 0)
					{
						printf("Error : Server accpet failed.\n");
						break;
					}
					else if (clientSocket > 99)
					{
						close (clientSocket);
					}
					else
					{
						FD_SET(clientSocket, &active_set);
						
						if(clientSocket > sock_max)
							sock_max = clientSocket;
						uint8_t op = 0x0;
						uint8_t proto = 0x0;
						uint16_t checksum = 0x0;
						uint32_t trans_id = 0x0;
						/* read TCP info */
						uint8_t pack_buff;
						recv(clientSocket, &op, 1, 0);
					    recv(clientSocket, &proto, 1, 0);
					    recv(clientSocket, &pack_buff, 1, 0);
					    checksum = ((uint16_t) pack_buff);
					    recv(clientSocket, &pack_buff, 1, 0);
					    checksum += ((uint16_t) pack_buff << 8);
					    //read(clientSocket, &checksum, 2);
					    //read(clientSocket, &trans_id, 4);
					    recv(clientSocket, &pack_buff, 1, 0);
					    trans_id = ((uint16_t) pack_buff);
					    recv(clientSocket, &pack_buff, 1, 0);
					    trans_id += ((uint16_t) pack_buff << 8);
					    recv(clientSocket, &pack_buff, 1, 0);
					    trans_id += ((uint16_t) pack_buff << 16);
					    recv(clientSocket, &pack_buff, 1, 0);
					    trans_id += ((uint16_t) pack_buff << 24);
						printf("op : %02x, pt : %02x, checksum : %04x, id : %08x\n", op, proto, checksum, trans_id);
						uint32_t sum = (trans_id&0xffff) + ((trans_id >> 16)&0xffff);
						sum += ((uint32_t)op) + ((uint32_t)proto << 8);
						sum = sum + (sum >> 16);
						uint16_t check = ~sum;
						
						printf("check : 0x%04x\n", check);
						if(checksum != check)
						{
							printf("Reject Connection number : %d\n", clientSocket);
							close(clientSocket);
							FD_CLR (clientSocket, &active_set);
							exit(1);
						}
						printf("success to connect Client\n");
						/* re send TCP */
						time_t t;
						if(proto == 0x0)
						{
							srand((unsigned) time(&t));
							proto = (rand() % 2)+ 1;
						}

						printf("proto : %d \n", proto);

						//printf("%d prp\n", proto);
						client_list[clientSocket].proto = proto;
						//printf("%d prp\n", proto);
						//printf("%d prp\n", client_list[clientSocket].proto);
						client_list[clientSocket].send_buff = (char *)malloc (10485760);
						bzero(client_list[clientSocket].send_buff, PACKET_LANGE+1);
						memset(client_list[clientSocket].send_buff, 0, 10000);
						client_list[clientSocket].p1_passed = 1;
						client_list[clientSocket].length = 0;
						client_list[clientSocket].count = 0;
						client_list[clientSocket].even = 0;
						client_list[clientSocket].k = 0;
						client_list[clientSocket].lastword = '\0';

						//printf("%d\n", client_list[clientSocket].proto);

						op = 1;
						write(clientSocket, &op, 1);
						write(clientSocket, &proto, 1);
						write(clientSocket, &checksum, 2);
						write(clientSocket, &trans_id, 4);
					}
				}
				else
				{
					//printf("dfsdfsefsf : %d \n", client_list[i].p1_passed);
					if(client_list[i].p1_passed >= 1)
					{
						printf("aaaaaaaaaaaaaaa\n");
						uint8_t proto = client_list[i].proto;
						char lastword = client_list[i].lastword;
						int length = client_list[i].length;
						int count = client_list[i].count;
						int even = client_list[i].even;
						int k = client_list[i].k;
						int length_rcv;
						int j, l;

						//printf("%d proto : %d\n", i, proto);

						memset(buff_rcv, 0, 10000);
						
						switch(proto)
						{
							case 1:
							//printf("liyliy", len);
							len = recv(i, buff_rcv, 8700, 0);
							printf("re len : %d\n", len);
							if(len == 0)
							{
								printf("Error : Connection Close\n");
								close(i);
								exit(1);
							}

							for(l = 0, j = k; l < len; l++)
							{
								//printf("%d %d %d\n", l, j, len);
								if(lastword == '\\')
								{
									//printf("sdf : %s\n", &buff_sd[0]);
									if(even == 0)
									{
										//printf("?! : %d\n", 1);
										client_list[i].even = 1;
										even = 1;
									}
									else
									{
										client_list[i].even = 0;
										even = 0;
									}
									if(buff_rcv[l] == '0')
									{
										//printf("?? : %d\n", even);
										if(even == 1)
										{
											buff_sd[j++] = buff_rcv[l];
											//printf("i send tou : %d %s\n", k+2, buff_sd);
											write(i, buff_sd, k+2);
											bzero(buff_sd, sizeof(buff_sd));
											//close(i);
											j = 0;
											k = 0;
											even = 0;
											client_list[i].even = 0;
											client_list[i].k = 0;
											lastword = '0';
											client_list[i].lastword = lastword;
											continue;
										}
										
									}
									if(buff_rcv[l] == '\\')
									{
										if(count == 0)
										{
											//client_list[i].send_buff[k] = buff_rcv[l];
											buff_sd[k] = buff_rcv[l];
											j += 1;
											//client_list[i].k += 1;
											k += 1;
											client_list[i].k = k;
											client_list[i].count = 1;
											count = 1;
										}							
									}
								}
								if(lastword != buff_rcv[l])
								{

									//printf("l : %c b : %c\n", lastword, buff_rcv[l]);

									if(even == 1)
									{
										printf("Error : Odd Escape\n");
										close(i);
										exit(1);
										break;
									}
									//client_list[i].send_buff[k] = buff_rcv[l];
									buff_sd[k] = buff_rcv[l];
									//printf("l : %c b : %c\n", buff_sd[k], buff_rcv[l]);
									//printf("%s\n", buff_sd);
									//printf("%s\n", &client_list[i].send_buff[0]);
									j += 1;
									//client_list[i].k += 1;
									k += 1;
									client_list[i].k = k;
									//printf("%s\n", &buff_sd[j]);
									lastword = buff_rcv[l];

									client_list[i].lastword = lastword;

									count = 0;
									client_list[i].count = 0;

									even = 0;
									client_list[i].even = 0;

									//printf("%d %d %d\n", j, k, l);
								}
								if(k > 9000)
								{
									write(i, buff_sd, k);
									bzero(buff_sd, sizeof(buff_sd));
									bzero(client_list[i].send_buff, sizeof(buff_sd));
									j = 0;
									k = 0;
									client_list[i].k = 0;
								}
							}

							//printf("sdfsdf : %d\n", k);
							//write(i, buff_sd, k);
				
							bzero(buff_rcv, sizeof(buff_rcv));
						
							break;
							case 2:
								break;
							/*len = recv(clientSocket, &length_rcv, 4, 0);
							length_rcv = ntohl(length_rcv);
							printf("len : %d length : %d\n", (int)len, length_rcv);	
							j = 0;
							while((length_rcv > 0) && (len = recv(clientSocket, buff_rcv, 10000, 0)))	
								{
								printf("second %d\n", (int)len);	
								length_rcv -= len;	
								for(i = 0; i < len; i++)
								{
									if(lastword != buff_rcv[i])
									{
										buff_sd[j++] = buff_rcv[i];
										lastword = buff_rcv[i];
										length = j+1;
									}
								}
								
								bzero(buff_rcv, sizeof(buff_rcv));
								
							}
								uint32_t length_send = htonl(length);
								write(clientSocket, &length_send, sizeof(length_send));
								write(clientSocket, buff_sd, length);
								printf("len : %d\n%s\n",length, buff_sd);
								bzero(buff_sd, sizeof(buff_sd));
								break;*/
						}
					}
				}
			}
		}

	}	
	
	


	return 0;
}
/*
void clientProcess(int clientSocket)
{
	uint8_t op = 0x0;
	uint8_t proto = 0x0;
	uint16_t checksum = 0x0;
	uint32_t trans_id = 0x0;


	uint8_t pack_buff;
	read(clientSocket, &op, 1);
    read(clientSocket, &proto, 1);
    read(clientSocket, &pack_buff, 1);
    checksum = ((uint16_t) pack_buff);
    read(clientSocket, &pack_buff, 1);
    checksum += ((uint16_t) pack_buff << 8);
    //read(clientSocket, &checksum, 2);
    //read(clientSocket, &trans_id, 4);
    read(clientSocket, &pack_buff, 1);
    trans_id = ((uint16_t) pack_buff);
    read(clientSocket, &pack_buff, 1);
    trans_id += ((uint16_t) pack_buff << 8);
    read(clientSocket, &pack_buff, 1);
    trans_id += ((uint16_t) pack_buff << 16);
    read(clientSocket, &pack_buff, 1);
    trans_id += ((uint16_t) pack_buff << 24);

	printf("op : %02x, pt : %02x, checksum : %04x, id : %08x\n", op, proto, checksum, trans_id);

	uint32_t sum = (trans_id&0xffff) + ((trans_id >> 16)&0xffff);
	sum += ((uint32_t)op) + ((uint32_t)proto << 8);
	sum = sum + (sum >> 16);

	uint16_t check = ~sum;
	

	printf("check : 0x%04x\n", check);

	if(checksum != check)
	{
		printf("Reject Connection number : %d\n", clientSocket);
		close(clientSocket);
		exit(1);
	}

	printf("success to connect Client\n");

	time_t t;

	if(proto == 0x0)
	{
		srand((unsigned) time(&t));
		proto = (rand() % 2)+ 1;
	}

	op = 1;
	write(clientSocket, &op, 1);
	write(clientSocket, &proto, 1);
	write(clientSocket, &checksum, 2);
	write(clientSocket, &trans_id, 4);

	char* buff_sd = (char *)malloc (10485760);
	char* buff_rcv = (char *)malloc (10240);
	bzero(buff_rcv, PACKET_LANGE+1);
	bzero(buff_sd, PACKET_LANGE+1);

	size_t len;
	int i, j;
	int k = 0;
	char lastword = '\0';
	int count;	
	int length;
	int even;

	printf("start read\n");
	memset(buff_rcv, 0, sizeof(buff_rcv));
	memset(buff_sd, 0, sizeof(buff_sd));


	int length_rcv;

	switch(proto)
		{
			length = 0;
			count = 0;
			even = 0;
			lastword = '\0';
			k = 0;
			memset(buff_rcv, 0, 10000);
			memset(buff_sd, 0, 10000);

			case 1:
			while(1)
			{
				len = recv(clientSocket, buff_rcv, 10000, 0);

				if(len == 0)
				{
					printf("Error : Connection Close\n");
					close(clientSocket);
					exit(1);
				}

				for(i = 0, j = k; i < len; i++)
				{
					if(lastword == '\\')
					{
						if(even == 0)
							even = 1;
						else
							even = 0;

						if(buff_rcv[i] == '0')
						{
							if(even == 1)
							{
								write(clientSocket, buff_sd, k);
								bzero(buff_sd, sizeof(buff_sd));
								close(clientSocket);
								j = 0;
								k = 0;
								even = 0;
							}
							
						}
						if(buff_rcv[i] == '\\')
						{
							if(count == 0)
							{
								buff_sd[j] = buff_rcv[i];
								j += 1;
								k += 1;
								count = 1;
							}							
						}
					}

					if(lastword != buff_rcv[i])
					{
						if(even == 1)
						{
							printf("Error : Odd Escape\n");
							close(clientSocket);
							exit(1);
							break;
						}
						buff_sd[j] = buff_rcv[i];
						j += 1;
						k += 1;
						//printf("%s\n", &buff_sd[j]);
						lastword = buff_rcv[i];
						count = 0;
						even = 0;
					}

					if(k > 9000)
					{
						write(clientSocket, buff_sd, k);
						bzero(buff_sd, sizeof(buff_sd));
						j = 0;
						k = 0;
					}
				}
	
				bzero(buff_rcv, sizeof(buff_rcv));
			}
				break;
			case 2:
			len = recv(clientSocket, &length_rcv, 4, 0);
			length_rcv = ntohl(length_rcv);
			printf("len : %d length : %d\n", (int)len, length_rcv);	
			j = 0;
			while((length_rcv > 0) && (len = recv(clientSocket, buff_rcv, 10000, 0)))	
				{
				printf("second %d\n", (int)len);	
				length_rcv -= len;			

				//length = 0;

				for(i = 0; i < len; i++)
				{
					if(lastword != buff_rcv[i])
					{
						buff_sd[j++] = buff_rcv[i];
						lastword = buff_rcv[i];
						length = j+1;
					}
				}

				
				bzero(buff_rcv, sizeof(buff_rcv));
				
			}
				uint32_t length_send = htonl(length);
				write(clientSocket, &length_send, sizeof(length_send));
				write(clientSocket, buff_sd, length);

				printf("len : %d\n%s\n",length, buff_sd);

				bzero(buff_sd, sizeof(buff_sd));

				break;
		}
	
	free(buff_sd);
	free(buff_rcv);

	printf("end of protocol\n");

	close(clientSocket);
}
*/