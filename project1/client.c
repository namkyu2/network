#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <unistd.h>

#include <inttypes.h> // to print uint64_t
#include <stdint.h>

#define PACKET_LANGE 64

struct packet
{
	uint8_t op;
	uint8_t proto;
	uint16_t checksum;
	uint32_t trans_id;
};

uint16_t calc_checksum(uint8_t op, uint8_t proto, uint32_t trans_id)
{
	uint32_t sum = (trans_id&0xffff) + ((trans_id >> 16)&0xffff);
	sum += ((uint32_t)op) + ((uint32_t)proto << 8);
	sum = sum + (sum >> 16);
	return ~sum;
}

int main(int argc, char *argv[])
{
	// Argument Value
	int param_opt;
	int is_h = 0;
	int is_m = 0;
	int is_p = 0;
	int unknown = 0;
	char *port_arg = "31415";
	char *hs_addr = "143.248.48.110";
	char *pt_arg = "1";

	while((param_opt = getopt(argc, argv, "h:m:p:")) != -1)
	{
		switch(param_opt)
		{
			case 'p':
				is_p = 1;
				port_arg = optarg;
				//printf( "port : %s\n", port_arg);
				break;
			case 'h':
				is_h = 1;
				hs_addr = optarg;
				//printf( "addr : %s\n", hs_addr);
				break;
			case 'm':
				is_m = 1;
				pt_arg = optarg;
				//printf( "protocol : %s\n", pt_arg);
				break;
			case '?':
				unknown = 1;
				//printf("unknown option : %c\n", optopt);
				break;
		}
	}

	
	uint8_t proto = 0x01;
	proto = (int) strtol (pt_arg, NULL, 10);

	if (proto != 0 && proto != 1 && proto != 2)
	{
		printf( "Error : protocol must be 0 or 1 or 2\n");
		exit(1);
	}

	uint8_t op = 0x00;
	uint32_t trans_id = 0xffff;

	uint16_t checksum = calc_checksum(op,proto,trans_id);

	int clientSocket;	// socket descriptor
	struct sockaddr_in server_addr;	// add, port assign

	char buff_send[PACKET_LANGE];
	char buff_rcv[PACKET_LANGE];


	if((clientSocket = socket(AF_INET, SOCK_STREAM, 0)) == -1)		// AF - Address, PF - Protocol, But same value
	{
		printf("Error : Client can't init Socket\n");
		exit(0);
	}

	memset(&server_addr, 0, sizeof(server_addr));

	server_addr.sin_family = AF_INET;
	//server_addr.sin_port = htons(PORT);
	//server_addr.sin_addr.s_addr = htonl(INADDR_ANY);	// htonl( INADDR_ANY)  is set address of client computer.
	//server_addr.sin_addr.s_addr = inet_addr(argv[1])

	/* Sample server for CS341 */
	//server_addr.sin_port = htons(31415);
	server_addr.sin_port = htons(atoi(port_arg));
	server_addr.sin_addr.s_addr = inet_addr(hs_addr);
	//server_addr.sin_addr.s_addr = inet_addr(143.248.48.110);


	if(connect(clientSocket, (struct sockaddr*) &server_addr, sizeof(server_addr)) == -1)
	{
		printf("Error : Client can't connect to server\n");
		exit(0);
	}

	//Phase 1
	struct send_packet; 
	struct recv_packet; 

	uint8_t pack_buff;

	write(clientSocket, &op, 1);
	write(clientSocket, &proto, 1);
	write(clientSocket, &checksum, 2);
	write(clientSocket, &trans_id, 4);
	
	//printf("op : %02x, pt : %02x, checksum : %04x, id : %08x\n", op, proto, checksum, trans_id);


	op = 0;
	proto = 0;
	checksum = 0;
	trans_id = 0;
	read(clientSocket, &op, 1);
	read(clientSocket, &proto, 1);
	read(clientSocket, &pack_buff, 1);
	checksum = ((uint16_t) pack_buff);
	read(clientSocket, &pack_buff, 1);
	checksum += ((uint16_t) pack_buff << 8);
	//read(clientSocket, &checksum, 2);
	//read(clientSocket, &trans_id, 4);
	read(clientSocket, &pack_buff, 1);
	trans_id = ((uint16_t) pack_buff);
	read(clientSocket, &pack_buff, 1);
	trans_id += ((uint16_t) pack_buff << 8);
	read(clientSocket, &pack_buff, 1);
	trans_id += ((uint16_t) pack_buff << 16);
	read(clientSocket, &pack_buff, 1);
	trans_id += ((uint16_t) pack_buff << 24);

	//printf("op : %02x, pt : %02x, checksum : %04x, id : %08x\n", op, proto, checksum, trans_id);

	
	

	// phase 2

	//printf("where?\n");

	if(proto == 1)
	{
		char packet_sd[10500];
		char packet_prc[10500];

		int count = 0;

		int k = 0;

		int i_k = 0;

		int MSG_count = 0;

		while((k = read(0, packet_prc, 10000)) > 0)
		{
			int i, j;

			for(i = 0, j = 0; i < k; i++)
			{
				packet_sd[j++] = packet_prc[i];
				if(packet_prc[i] == '\\'){
					/*if(packet_prc[i+1] != '0')	// 다중 메세지 테스트
					{		*/				
						packet_sd[j++] = packet_prc[i];
					/*} else
						MSG_count++;*/
				}
			}

			write(clientSocket, packet_sd, j);
			count++;

			i_k += j;
			//printf("sd - len : -- %d count : %d -- all : %d\n", j, count, i_k);
		}

		//printf("asdf\n");

		write(clientSocket, "\\0", 2);

		//MSG_count++;

		//printf("assdfdsfdf\n");

		char packet_recv[13020];

		uint32_t len = 1;
		char* str_t;


		//FILE *fp = fopen("result.out", "w");

		
		count = 0;

		i_k = 0;
		
		while(len = read(clientSocket, packet_recv, 10000))
		{
			int i = 0;
			//len = read(clientSocket, packet_recv, 1000);
			//str_t = strtok(packet_recv, "\\0");
			//str_t = strtok(NULL,"\\0");

			i_k += len;

			//printf("rc - len : %d -- count : %d -- all : %d\n", len, count++, i_k);

			//printf("%s\n",packet_recv);
			for(i = 0;( i < len) /*&& (MSG_count > 0)*/; i++)
			{
				if(packet_recv[i] == '\\')
				{
					if(packet_recv[i+1] == '\\')
					{
						write(1, &packet_recv[i], 1);
						//printf("%c", packet_recv[i]);
					}else if(packet_recv[i+1] == '0')
					{
						//printf("fuck you\n");
						//MSG_count--;
						i = -1;
						break;
					} 

					i++;
				}else
				{
					write(1, &packet_recv[i], 1);
					//printf("%c", packet_recv[i]);
				}
			}

			//printf("MSG : %d\n", MSG_count);

			if(i == -1 /*|| MSG_count == 0*/){
				//printf("why fuck you\n");
				break;
			}
		}
	} else if(proto == 2)
	{
		//printf("where?\n");
		int size = (int)lseek(0, 0, SEEK_END);
		lseek(0,0,SEEK_SET);

		if(size > 0)
		{

		int k1, k2, buff_size;
		char buff_buff[1050];

		k1 = 0;
		k2 = 0;
		buff_size = 0;

		bzero(buff_buff, 1000);

		//printf("where?\n");

		

		//printf("size : %d\n", size);

			size = htonl(size);

			write(clientSocket, &size, 4);


			while(k1 = read(0, buff_buff, 1000))
			{
				//printf("len : %d\n", k1);
				
				write(clientSocket, buff_buff, k1);

				//printf("all, done\n");
			}

			//read(clientSocket, &size, 4);

			int bu_size = 0;
			int opgg = 0;

			read(clientSocket, &opgg, 1);
		    bu_size = ((int) opgg);
		    read(clientSocket, &opgg, 1);
		    bu_size += ((int) opgg << 8);
		    read(clientSocket, &opgg, 1);
		    bu_size += ((int) opgg << 16);
		    read(clientSocket, &opgg, 1);
		    bu_size += ((int) opgg << 24);

			size = ntohl(bu_size);

			bu_size = size;
			if(size > 1000)
				bu_size = 1000;

			//printf("%d %d\n", size, bu_size);


			while((size > 0) && (k1 = read(clientSocket, buff_buff, bu_size)))
			{
				//size -= k1;
				//size -= k1;

				

				for(k2 = 0; (k2 < k1) && (size > 0); k2++)
				{
					write(1, &buff_buff[k2], 1);
					size--;
					//printf("sd : %d %d %d\n", size, k2, k1);
				}

				if(size > 1000)
					bu_size = 1000;
				else
					bu_size = size;

				//write(1, buff_buff, k1);

			}
		} else
		{
			char* buff_buff = (char *)malloc (10485760);
			char* buff_sd = (char *)malloc (10240);
			int jj = 0;
			int i = 0;
			int k = 0;
			int opgg = 0;

			while(k = read(0, buff_sd, 10000))
			{
				for(i = 0; i < k; i++)
				{
					buff_buff[jj++] = buff_sd[i];
				}
			}

			int bu_size = htonl(jj);

			write(clientSocket, &bu_size, 4);
			write(clientSocket, buff_buff, jj);

			bu_size = 0;
			read(clientSocket, &opgg, 1);
		    bu_size = ((uint16_t) opgg);
		    read(clientSocket, &opgg, 1);
		    bu_size += ((uint16_t) opgg << 8);
		    read(clientSocket, &opgg, 1);
		    bu_size += ((uint16_t) opgg << 16);
		    read(clientSocket, &opgg, 1);
		    bu_size += ((uint16_t) opgg << 24);

		    jj = ntohl(bu_size);

			//printf("sdfsdf - %d\n", jj);

		    while((jj > 0) && (k = read(clientSocket, buff_sd, 1000)))
		    {
		    	for(i = 0; (i < k) && (jj > 0); i++)
		    	{
		    		write(1, &buff_sd[i], 1);
		    		jj--;
		    	}
		    }

		}

	}

	

	close(clientSocket);
	

	return 0;
}


