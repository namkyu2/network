#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>

#define PACKET_LANGE 512

void clientProcess(int clientSocket);	// Server conneted to client, then process this function.

void sig_action_handler (int s)
{
	while (waitpid (-1, NULL, WNOHANG) > 0);
}

int main(int argc, char *argv[])
{
	int serverSocket;
	int clientSocket;

	int client_addr_size;

	struct sockaddr_in server_addr;
	struct sockaddr_in client_addr;
	socklen_t len;

	//Argument Value
	int param_opt;
	int is_h = 0;
	char *port_arg = NULL;
	//End

	struct sigaction sig_action;

	//Argument Parsing
	while((param_opt = getopt(argc, argv, "p:")) != -1)
	{
		switch(param_opt)
		{
			case 'p':
				is_h = 1;
				port_arg = optarg;
				printf( "port : %s\n", port_arg);
				break;
			case '?':
				printf("unknown option : %c\n", optopt);
				break;
		}
	}

	if (is_h == 0 || (port_arg == NULL))
	{
		printf( "Error : type -p port : %s\n", port_arg);
		exit(1);
	}
	//End

	//Server addr memory set
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(atoi(port_arg));
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);

	//Make Socket
	if((serverSocket = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		printf("Error : Server can't init Socket\n");
		exit(1);
	}
	
	//Binding Socket
	if(bind(serverSocket, (struct sockaddr*)&server_addr, sizeof(server_addr)) == -1)	// assign address & port on 
	{
		printf("Error : Server can't bind Socket\n");
		exit(1);
	}

	if(listen(serverSocket, 5) < 0)
    {
        printf("Error : Server Can't listen.\n");
        exit(1);
    }

    printf("Waiting Connection.\n");

    sig_action.sa_handler = sig_action_handler;
	sigemptyset (&sig_action.sa_mask);

	sig_action.sa_flags = SA_RESTART;
	if (sigaction (SIGCHLD, &sig_action, NULL) == -1)
	{
		printf("Error : Sigaction Problem\n");
		exit (1);
	}

	while(1)
	{
		len = sizeof(client_addr);
		clientSocket = accept (serverSocket, (struct sockaddr*)&client_addr, &len);

		if(clientSocket < 0)
		{
			printf("Error : Server accpet failed.\n");
			break;
		}

		int pid = fork();

		if(pid < 0)
		{
			printf("ERROR on fork\n");
			exit(1);
		}
		else if(pid == 0)
		{
			/* client process */
			close(serverSocket);
			clientProcess(clientSocket);
			//return 0;
			exit(0);
		} else // pid not 0
		{
			close(clientSocket);
		}

		close(clientSocket);
	}

	close(serverSocket);

	return 0;
}

void clientProcess(int clientSocket)
{
	uint8_t op = 0x0;
	uint8_t proto = 0x0;
	uint16_t checksum = 0x0;
	uint32_t trans_id = 0x0;

	/* read TCP info */

	uint8_t pack_buff;
	read(clientSocket, &op, 1);
    read(clientSocket, &proto, 1);
    read(clientSocket, &pack_buff, 1);
    checksum = ((uint16_t) pack_buff);
    read(clientSocket, &pack_buff, 1);
    checksum += ((uint16_t) pack_buff << 8);
    //read(clientSocket, &checksum, 2);
    //read(clientSocket, &trans_id, 4);
    read(clientSocket, &pack_buff, 1);
    trans_id = ((uint16_t) pack_buff);
    read(clientSocket, &pack_buff, 1);
    trans_id += ((uint16_t) pack_buff << 8);
    read(clientSocket, &pack_buff, 1);
    trans_id += ((uint16_t) pack_buff << 16);
    read(clientSocket, &pack_buff, 1);
    trans_id += ((uint16_t) pack_buff << 24);

	printf("op : %02x, pt : %02x, checksum : %04x, id : %08x\n", op, proto, checksum, trans_id);

	uint32_t sum = (trans_id&0xffff) + ((trans_id >> 16)&0xffff);
	sum += ((uint32_t)op) + ((uint32_t)proto << 8);
	sum = sum + (sum >> 16);

	uint16_t check = ~sum;
	

	printf("check : 0x%04x\n", check);

	if(checksum != check)
	{
		printf("Reject Connection number : %d\n", clientSocket);
		close(clientSocket);
		exit(1);
	}

	printf("success to connect Client\n");

	/* re send TCP */
	time_t t;

	if(proto == 0x0)
	{
		srand((unsigned) time(&t));
		proto = (rand() % 2)+ 1;
	}

	op = 1;
	write(clientSocket, &op, 1);
	write(clientSocket, &proto, 1);
	write(clientSocket, &checksum, 2);
	write(clientSocket, &trans_id, 4);

	char* buff_sd = (char *)malloc (10485760);
	char* buff_rcv = (char *)malloc (10240);
	bzero(buff_rcv, PACKET_LANGE+1);
	bzero(buff_sd, PACKET_LANGE+1);

	size_t len;
	int i, j;
	int k = 0;
	char lastword = '\0';
	int count;	
	int length = 0;
	int even;

	printf("start read\n");
	int length_rcv;

	count = 0;
	even = 0;
	lastword = '\0';
	k = 0;
	memset(buff_rcv, 0, 10000);
	memset(buff_sd, 0, 10000);

	switch(proto)
		{
			case 1:

			while(1)
			{
				len = recv(clientSocket, buff_rcv, 10000, 0);

				//printf("recv len : %d -%s\n", len, buff_rcv);

				if(len == 0)
				{
					printf("Error : Connection Close\n");
					close(clientSocket);
					exit(1);
				}

				

				for(i = 0, j = k; i < len; i++)
				{
					if(lastword == '\\')
					{
						if(even == 0)
							even = 1;
						else
							even = 0;

						if(buff_rcv[i] == '0')
						{
							if(even == 1)
							{
								buff_sd[j++] = buff_rcv[i];
								write(clientSocket, buff_sd, k+2);
								//printf("k : %d, j : %d\n", k, j);
								//printf("%s\n", buff_sd);
								bzero(buff_sd, sizeof(buff_sd));
								//close(clientSocket);
								j = 0;
								k = 0;
								even = 0;
								lastword = '0';
								continue;
								//lastword = '\0';
								//exit(1);
							}
							
						}
						if(buff_rcv[i] == '\\')
						{
							if(count == 0)
							{
								buff_sd[j] = buff_rcv[i];
								j += 1;
								k += 1;
								count = 1;
							}							
						}
					}

					if(lastword != buff_rcv[i])
					{
						if(even == 1)
						{
							printf("Error : Odd Escape\n");
							close(clientSocket);
							exit(1);
							break;
						}
						buff_sd[j] = buff_rcv[i];
						j += 1;
						k += 1;
						//printf("%s\n", &buff_sd[j]);
						lastword = buff_rcv[i];
						count = 0;
						even = 0;
					}

					if(k > 9000)
					{
						write(clientSocket, buff_sd, k);
						bzero(buff_sd, sizeof(buff_sd));
						j = 0;
						k = 0;
					}
				}
	
				bzero(buff_rcv, sizeof(buff_rcv));
			}
				break;
			case 2:

			//printf("%i\n", length);
			while(1)
			{
				int bu_size = 0;
				int opgg = 0;

				if(read(clientSocket, &opgg, 1))
			    	bu_size = ((int) opgg);
			    else
			    	break;
			    if(read(clientSocket, &opgg, 1))
			    	bu_size += ((int) opgg << 8);
			    else
			    	break;
			    if(read(clientSocket, &opgg, 1))
			    	bu_size += ((int) opgg << 16);
			    else 
			    	break;
			    if(read(clientSocket, &opgg, 1))
			    	bu_size += ((int) opgg << 24);
			    else
			    	break;

				length_rcv = ntohl(bu_size);
				printf("len : %d length : %d\n", (int)len, length_rcv);	
				j = 0;

				length = 0;

				if(length_rcv == 0)
				{
					break;
				}

				while((length_rcv > 0))	
					{
					if(length_rcv >= 10000){
						len = recv(clientSocket, buff_rcv, 10000, 0);
					}
				
					else
						len = recv(clientSocket, buff_rcv, length_rcv, 0);

					printf("second %d\n", (int)len);

					if(len == 0)
					{
						printf("Error : no character\n");
						close(clientSocket);
						exit(1);
						break;
					}

					length_rcv -= len;	
					if(length_rcv < 0)
					{
						printf("Error : more character\n");
						close(clientSocket);
						exit(1);
						break;
					}		

					//length = 0;

					for(i = 0; i < len; i++)
					{
						if(lastword != buff_rcv[i])
						{
							buff_sd[j++] = buff_rcv[i];
							lastword = buff_rcv[i];
							length += 1;
						}
					}

					bzero(buff_rcv, sizeof(buff_rcv));
					
				}
					uint32_t length_send = htonl(length);
					write(clientSocket, &length_send, sizeof(length_send));
					write(clientSocket, buff_sd, length);

					//printf("len : %d\n%s\n",length, buff_sd);

					bzero(buff_sd, sizeof(buff_sd));

			}

			break;
		}
	
	free(buff_sd);
	free(buff_rcv);

	printf("end of protocol\n");

	close(clientSocket);
}
