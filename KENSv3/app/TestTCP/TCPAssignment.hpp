/*
 * E_TCPAssignment.hpp
 *
 *  Created on: 2014. 11. 20.
 *      Author: 근홍
 */

#ifndef E_TCPASSIGNMENT_HPP_
#define E_TCPASSIGNMENT_HPP_


#include <E/Networking/E_Networking.hpp>
#include <E/Networking/E_Host.hpp>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <netinet/ip.h>
#include <netinet/in.h>

#include <queue>
#include <vector>


#include <E/E_TimerModule.hpp>

#define MAX_SOCK 20

 
namespace E
{

typedef struct pseudoHeader
{
	public:
		short src_port;	// uint16_t
		short dst_port;
		int seq_num;
		int ack_num;

		/*Optional */
		uint8_t hlen;
		uint8_t urg;
		uint8_t ack;
		uint8_t psh;
		uint8_t rst;
		uint8_t syn;
		uint8_t fin;
		short window_size;
		short checksum;
		short urg_ptr;
} pseudoHeader;

typedef struct TcpHeader
{
	public:
		short src_port;	// uint16_t
		short dest_port;
		int seq_num;
		int ack_num;

		/*Optional */
		uint8_t offset;
		uint8_t flag;
		short window_size;
		short checksum;
		short urg_ptr;
} TcpHeader;

class TCPAssignment : public HostModule, public NetworkModule, public SystemCallInterface, private NetworkLog, private TimerModule
{
private:

private:
	virtual void timerCallback(void* payload) final;

public:
	TCPAssignment(Host* host);
	virtual void initialize();
	virtual void finalize();
	virtual ~TCPAssignment();
protected:
	virtual void syscall_socket(UUID syscallUUID, int pid, int domain, int type, int protocol) final;
	virtual void syscall_close(UUID syscallUUID, int pid, int sockfd) final;
	virtual void syscall_read(UUID syscallUUID, int pid, int fd, void* buf, size_t count) final;
	virtual void syscall_write(UUID syscallUUID, int pid, int fd, void* buf, size_t count) final;
	virtual void syscall_connect(UUID syscallUUID, int pid, int sockfd, struct sockaddr* servaddr, socklen_t addrlen) final;
	virtual void syscall_listen(UUID syscallUUID, int pid, int sockfd, int backlog) final;
	virtual void syscall_accept(UUID syscallUUID, int pid, int sockfd, struct sockaddr* cliaddr, socklen_t* addrlen) final;
	virtual void syscall_bind(UUID syscallUUID, int pid, int sockfd, const struct sockaddr* servaddr, socklen_t addrlen) final;
	virtual void syscall_getsockname(UUID syscallUUID, int pid, int sockfd, struct sockaddr* addr, socklen_t* addrlen) final;
	virtual void syscall_getpeername(UUID syscallUUID, int pid, int sockfd, struct sockaddr* addr, socklen_t* addrlen) final;
	virtual void systemCallback(UUID syscallUUID, int pid, const SystemCallParameter& param) final;
	//virtual Packet* makePacket(Packet* packet) final;
	virtual Packet* makePacket(Packet* myPacket, uint8_t *temp, uint32_t* src_ip, uint32_t* dst_ip, int length) final;
	virtual void send_writebuffer(int pid, int sockfd) final;
	virtual void packetArrived(std::string fromModule, Packet* packet) final;
};


class TCPAssignmentProvider
{
private:
	TCPAssignmentProvider() {}
	~TCPAssignmentProvider() {}
public:
	static HostModule* allocate(Host* host) { return new TCPAssignment(host); }
};

typedef struct Pakcet_Window
{
	public:
		int seq;
		int ack;
		int ex_ack;
		int ex_seq;
		bool accept;

} Pakcet_Window;

class SocketInfo
{
	public:
		UUID sock_UUID;
		UUID accept_wait_UUID;
		UUID accept_timer_UUID;
		int socket_fd;
		bool is_servSock;
		int pid;
		int state;

		uint32_t seq_sent;
		uint32_t ack_sent;

		uint32_t seq_recv;
		uint32_t ack_recv;

		uint32_t now_seq;
		uint32_t now_ack;

		uint32_t current_seq;
		uint32_t current_ack;

		bool syn, ack;

		/*Bind related variable*/
		bool isBinded;

		/*src addr information*/
		uint8_t sa_family;
		uint16_t sin_port;
		struct in_addr sin_addr;

		/*dst addr_information*/
		uint8_t dst_sa_family;
		uint16_t dst_sin_port;
		struct in_addr dst_sin_addr;

		/* TCP header */
		struct TcpHeader header;

		/*Listen related variable*/
		int backlog;
		int backlog_use;
		int listen_fd;

		Packet* packet;

		/* read write related variable*/
		uint8_t read_buffer[65535];
		uint8_t write_buffer[65535];

		int read_offset;
		int write_offset;

		UUID readcall_delay_UUID;
		void* read_delayed_buffer;
		int read_count;

		UUID writecall_delay_UUID;
		void* write_delayed_buffer;
		int write_count;
		bool packet_sending;
		bool send_complete;

		int window_size;
		int processing;
		int received_packet;
		int window_buffer[600];
		int sending_seq;

		//std::vector<Pakcet_Window> packet_window;

		// double RTT;
		// double TIME_WAIT;
		// int K;
		// double alpha;
		// double beta;
		int data_range;
};

} 


#endif /* E_TCPASSIGNMENT_HPP_ */
