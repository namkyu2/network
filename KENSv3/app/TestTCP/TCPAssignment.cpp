/*
* E_TCPAssignment.cpp
*
*  Created on: 2014. 11. 20.
*      Author: 근홍
*/


#include <E/E_Common.hpp>
#include <E/Networking/E_Host.hpp>
#include <E/Networking/E_Networking.hpp>
#include <cerrno>
#include <E/Networking/E_Packet.hpp>
#include <E/Networking/E_NetworkUtil.hpp>
#include "TCPAssignment.hpp"
#include "string.h"

namespace E
{


#define SOCK_CLOSED 	0
#define	SOCK_LISTEN 	1
#define	SYN_RCVD 		2
#define	SYN_SENT		3
#define ESTABLISHED		4
#define	CLOSE_WAIT		5
#define	LAST_ACK		6
#define	FIN_WAIT_1		7
#define	FIN_WAIT_2		8
#define	SOCK_CLOSING	9
#define	TIMED_WAIT		10
#define FIN_ACK_WAIT    11

#define TEMP_SEQ		0


#define FIN 	0x01
#define SYN 	0x02
#define RST	0x04
#define PSH 	0x08
#define ACK 	0x10	// 10
#define URG 	0x20	// 20

#define BUFFER_MAX 51200
#define WINDOW_LEN 512

    typedef std::unordered_map<std::pair<int, int>, SocketInfo*> SockMap; //first pid, second fd
    typedef std::unordered_map<std::pair<int, int>, SocketInfo*> TimedWaitSockMap;
    SockMap sockMap;
    TimedWaitSockMap TimedWaitsockMap;


    TCPAssignment::TCPAssignment(Host* host) : HostModule("TCP", host),
        NetworkModule(this->getHostModuleName(), host->getNetworkSystem()),
        SystemCallInterface(AF_INET, IPPROTO_TCP, host),
        NetworkLog(host->getNetworkSystem()),
        TimerModule(host->getSystem())
    {

    }

    int get_port()
    {
        int port_get = 0;
        bool asdf = true;

        for (port_get = 50000; ; port_get++)
        {
            asdf = true;
            for (auto iterSock = sockMap.begin(); iterSock != sockMap.end(); iterSock++)
            {
                if (iterSock->second->sin_port == port_get)
                {
                    asdf = false;
                    break;
                }
            }

            if (asdf)
                return port_get;

        }

        return -1;
    }

    auto find_TCP_socket(struct sockaddr_in src_addr, struct sockaddr_in dst_addr, uint8_t syn)
    {

        char str[INET_ADDRSTRLEN];

        // inet_ntop(AF_INET, &(src_addr.sin_addr), str, INET_ADDRSTRLEN);
        // printf("src : %s - %d  ", str, src_addr.sin_port);
        // inet_ntop(AF_INET, &(dst_addr.sin_addr), str, INET_ADDRSTRLEN);
        // printf("dst : %s - %d\n", str, dst_addr.sin_port);

        //printf("find port num : %d, %d\n", dst_addr.sin_port, src_addr.sin_port);

        for (auto iter_sock = sockMap.begin(); iter_sock != sockMap.end(); iter_sock++)
        {
            if (iter_sock->second->isBinded)
            {
                inet_ntop(AF_INET, &(iter_sock->second->sin_addr), str, INET_ADDRSTRLEN);
                //printf("iter : -%d-  %s - dst : ",iter_sock->first, str);
                inet_ntop(AF_INET, &(iter_sock->second->dst_sin_addr), str, INET_ADDRSTRLEN);
                //printf("%s \n", str);
                if (iter_sock->second->sin_addr.s_addr == dst_addr.sin_addr.s_addr ||
                    iter_sock->second->sin_addr.s_addr == INADDR_ANY)
                {
                    //printf("you in here\n");
                    if (iter_sock->second->sin_port == dst_addr.sin_port)
                    {
                        if (iter_sock->second->state == SOCK_LISTEN && syn)	// if that is listen, syn must be 1
                        {	// listen and syn is accept

                            return iter_sock;
                        }
                        else if (iter_sock->second->dst_sin_addr.s_addr == src_addr.sin_addr.s_addr &&
                            iter_sock->second->dst_sin_port == src_addr.sin_port)										// all setting is done.
                        {	// all of contains is correct
                            return iter_sock;
                        }
                    }
                }
            }
        }

        //@ryang 귀찮아서 위에꺼 복붙 TimeWaitsockMap에서 찾는 Func
        for (auto iter_sock = TimedWaitsockMap.begin(); iter_sock != TimedWaitsockMap.end(); iter_sock++)
        {
        	 if (iter_sock->second->isBinded)
           	{
               inet_ntop(AF_INET, &(iter_sock->second->sin_addr), str, INET_ADDRSTRLEN);
               //printf("iter : -%d-  %s - dst : ",iter_sock->first, str);
               inet_ntop(AF_INET, &(iter_sock->second->dst_sin_addr), str, INET_ADDRSTRLEN);
               //printf("%s \n", str);
               if (iter_sock->second->sin_addr.s_addr == dst_addr.sin_addr.s_addr ||
                   iter_sock->second->sin_addr.s_addr == INADDR_ANY)
               	{
               	    //printf("you in here\n");
               	    if (iter_sock->second->sin_port == dst_addr.sin_port)
               	    {
               	        if (iter_sock->second->state == SOCK_LISTEN && syn)	// if that is listen, syn must be 1
               	        {	// listen and syn is accept
               	            return iter_sock;
               	        }
               	        else if (iter_sock->second->dst_sin_addr.s_addr == src_addr.sin_addr.s_addr &&
               	            iter_sock->second->dst_sin_port == src_addr.sin_port)										// all setting is done.
               	        {	// all of contains is correct
               	            return iter_sock;
               	        }
               	    }
               	}
           	}
        }

        //printf("OMG I can't found\n");

        return sockMap.end();
    }

    TCPAssignment::~TCPAssignment()
    {

    }

    void TCPAssignment::initialize()
    {

    }

    void TCPAssignment::finalize()
    {

    }

    void do_bind(int pid, int sockfd, const struct sockaddr* servaddr, socklen_t addrlen)
    {
        auto sock_iter = sockMap.find(std::make_pair(pid, sockfd));

        if (sock_iter == sockMap.end())
            return;

        if (sock_iter->second->isBinded)
            return;

        for (auto iter_temp = sockMap.begin(); iter_temp != sockMap.end(); iter_temp++)
        {
            if (iter_temp == sock_iter)	// me...
                continue;

            if (iter_temp->second->sin_port == ((struct sockaddr_in*)servaddr)->sin_port)
            {
                if ((iter_temp->second->sin_addr.s_addr == ((struct sockaddr_in*)servaddr)->sin_addr.s_addr) || (iter_temp->second->sin_addr.s_addr == htonl(INADDR_ANY)))
                {
                    return;
                }
            }
        }

        sock_iter->second->sa_family = servaddr->sa_family;
        sock_iter->second->sin_addr.s_addr = ((struct sockaddr_in*)servaddr)->sin_addr.s_addr;
        sock_iter->second->sin_port = ((struct sockaddr_in*)servaddr)->sin_port;
        sock_iter->second->isBinded = true;

        char str[INET_ADDRSTRLEN];

        inet_ntop(AF_INET, &(sock_iter->second->sin_addr), str, INET_ADDRSTRLEN);

        //printf("success bind : %d fam :  %d port :  %d add : %s\n", sockfd, sock_iter->second->sa_family, sock_iter->second->sin_port, str);

        return;
    }

    void TCPAssignment::syscall_socket(UUID syscallUUID, int pid, int domain, int type, int protocol)
    {
        int fd = this->createFileDescriptor(pid);

        if (fd == -1)
        {
            this->returnSystemCall(syscallUUID, -1);
            return;
        }

        //printf("CREATEd fd %d - pid: %d\n", fd, pid);

        SocketInfo* new_socket = new SocketInfo()/*[sizeof(socketinfo)]*/;
        new_socket->sock_UUID = syscallUUID;
        new_socket->socket_fd = fd;
        new_socket->listen_fd = -1;
        new_socket->pid = pid;
        new_socket->isBinded = false;

        //@ryang offset 초기화
        new_socket->read_offset = 0;
        new_socket->write_offset = 0;
        new_socket->readcall_delay_UUID = -1;
        new_socket->sending_seq = TEMP_SEQ+1;

        sockMap.insert(SockMap::value_type(std::make_pair(pid, fd), new_socket));

        this->returnSystemCall(syscallUUID, fd);
    }

    void TCPAssignment::syscall_close(UUID syscallUUID, int pid, int sockfd)
    {
        auto sock_iter = sockMap.find(std::make_pair(pid, sockfd));

        if (sock_iter != sockMap.end())
        {
            if (sock_iter->second->state == ESTABLISHED || sock_iter->second->state == CLOSE_WAIT)
            {
                //printf("start close Call %d %d\n", sockfd, pid);
                uint32_t src_ip[4];
                uint32_t dest_ip[4];

                uint16_t src_port[2];
                uint16_t dest_port[2];
                uint8_t TCPFlags[1];
                uint32_t seq_num[4];
                uint32_t ack_num[4];

                uint8_t off_reserve[1];
                uint16_t window[2];
                uint16_t urgent[2];

                memset(seq_num, 0, sizeof(seq_num));
                memset(ack_num, 0, sizeof(ack_num));

                dest_ip[0] = sock_iter->second->dst_sin_addr.s_addr;
                dest_port[0] = sock_iter->second->dst_sin_port;

                src_ip[0] = sock_iter->second->sin_addr.s_addr;
                src_port[0] = sock_iter->second->sin_port;

                //printf("hihi %d\n", ntohl(sock_iter->second->now_seq));
                seq_num[0] = htonl(sock_iter->second->current_seq + sock_iter->second->data_range);
                ack_num[0] = 0;
                off_reserve[0] = 0x50;

                window[0] = htons(51200);
                urgent[0] = 0;

                TCPFlags[0] = FIN;

                uint8_t temp[20];	// it will be header
                memset(temp, 0, sizeof(temp));

                memcpy(&temp[0], src_port, 2);
                memcpy(&temp[2], dest_port, 2);
                memcpy(&temp[4], seq_num, 4);
                memcpy(&temp[8], ack_num, 4);
                memcpy(&temp[12], off_reserve, 1);
                memcpy(&temp[13], TCPFlags, 1);
                memcpy(&temp[14], window, 2);
                memcpy(&temp[18], urgent, 2);

                Packet* packet = allocatePacket(54);

                this->makePacket(packet, temp, src_ip, dest_ip, 20);

                this->sendPacket("IPv4", packet);

                //returnSystemCall(syscallUUID, 0);


                if(sock_iter->second->state == ESTABLISHED)
                	sock_iter->second->state = FIN_WAIT_1;
                else if(sock_iter->second->state == CLOSE_WAIT)
                	sock_iter->second->state = LAST_ACK;

                sock_iter->second->seq_sent = ntohl(seq_num[0]);
                sock_iter->second->sock_UUID = syscallUUID;
                //sockNow->second->packet = packet;
                //sockNow->second->Timerlist.push(this->addTimer(((void*)sockNow->second), 10000000000));
            }
            else {

               // printf("Just Close. %d %d\n", pid, sockfd);	
                this->removeFileDescriptor(pid, sockfd);
                delete sock_iter->second;
                sockMap.erase(std::make_pair(pid, sockfd));
                //printf("Map Size %d\n", sockMap.size());
                this->returnSystemCall(syscallUUID, 0);
                return;
            
                }

        }
        this->returnSystemCall(syscallUUID, -1);
    }

    void TCPAssignment::syscall_read(UUID syscallUUID, int pid, int sockfd, void* buf, size_t count)
    {
    	auto sock_iter = sockMap.find(std::make_pair(pid, sockfd));

    	if(sock_iter->second->read_offset > 0)
    	{
    		if(count > sock_iter->second->read_offset)
    			count = sock_iter->second->read_offset;

    		memcpy(buf, sock_iter->second->read_buffer, count);
    		memmove(sock_iter->second->read_buffer, sock_iter->second->read_buffer + count, sock_iter->second->read_offset - count);
    		sock_iter->second->read_offset -= count;

    		this->returnSystemCall(syscallUUID, count);
    	}
    	else
    	{
            //printf("block UUID : %d\n", syscallUUID);
            sock_iter->second->readcall_delay_UUID = syscallUUID;
            sock_iter->second->read_count = count;
            sock_iter->second->read_delayed_buffer = buf;
    	}
    }

    void TCPAssignment::syscall_write(UUID syscallUUID, int pid, int sockfd, void* buf, size_t count)
    {   // @namkyu : 어디부터 설명을 해야 할지 전혀 감이 잡히지 않는 이 세상에 우리가 있습니다.
        auto sock_iter = sockMap.find(std::make_pair(pid, sockfd));

        if(sock_iter->second->write_offset < BUFFER_MAX)
        {   // @namkyu : write_offset < BUFFER_MAX 말 안해도 알듯.
            if(sock_iter->second->write_offset == 0)
                sock_iter->second->packet_sending = false;
            // @namkyu : packet_sending 이라고 해서 현재 packet을 보내고 있는지 여부 확인에 쓰입니다.
            // @namkyu : offset 이 0 이면 당연히 보내고 있을 리가 없으니 기점으로 초기화.
            if(count + sock_iter->second->write_offset > BUFFER_MAX)
                count = BUFFER_MAX - sock_iter->second->write_offset;

            memcpy(sock_iter->second->write_buffer + sock_iter->second->write_offset, buf, count);
            sock_iter->second->write_offset += count;

            //printf("write : %d %d \n", count, sock_iter->second->write_offset);

            this->returnSystemCall(syscallUUID, count);
        }
        else
        {
            sock_iter->second->writecall_delay_UUID = syscallUUID;
            sock_iter->second->write_count = count;
            sock_iter->second->write_delayed_buffer = buf;
        }

        if(!sock_iter->second->packet_sending && sock_iter->second->write_offset > 0)
        {   // @namkyu : packet send를 위한 전초작업.
            sock_iter->second->window_size = 1; // @namkyu : 우리가 익히 아는 그 window size
            sock_iter->second->processing = -1; // @namkyu : 실제로 작업하고 있는 window_size를 뜻함. (밑에서 설정)
            sock_iter->second->packet_sending = true;

            send_writebuffer(pid, sockfd);
        }
    }

    void TCPAssignment::send_writebuffer(int pid, int sockfd)
    {   // @namkyu : write buffer의 모든 내용을 packet으로 보낼 친구.
        int count;
        // @namkyu : 어디서 많이 본 선언들. 이것들 전부 패킷을 만드는데 사용됨.
        struct sockaddr_in src_addr;
        struct sockaddr_in dst_addr;

        uint32_t src_ip[4];
        uint32_t dest_ip[4];

        /*TCP Header*/
        uint16_t src_port[2];
        uint16_t dest_port[2];
        uint8_t TCPFlags[1];
        uint32_t seq_num[4];
        uint32_t ack_num[4];

        uint8_t off_reserve[1];
        uint16_t window[2];
        uint16_t urgent[2];

        memset(&src_addr, 0, sizeof(src_addr));
        memset(&dst_addr, 0, sizeof(dst_addr));
        memset(seq_num, 0, sizeof(seq_num));
        memset(ack_num, 0, sizeof(ack_num));

        auto sock_iter = sockMap.find(std::make_pair(pid, sockfd));

        src_ip[0] = sock_iter->second->sin_addr.s_addr;
        src_port[0] = sock_iter->second->sin_port;
        dest_ip[0] = sock_iter->second->dst_sin_addr.s_addr;
        dest_port[0] = sock_iter->second->dst_sin_port;

        //printf("fd: %d window : %d offset : %d\n", sockfd, sock_iter->second->window_size, sock_iter->second->write_offset);

        sock_iter->second->processing = 0;          // @namkyu : 위에서 말한 프로세싱 사이즈.
        sock_iter->second->received_packet = 0;     // @namkyu : 아래와 함께 죽어야 하는 기능... 이지만 살아있음.
        sock_iter->second->send_complete = false;   // @namkyu : 죽은 기능. 무시.

        for(int i = 0; i < sock_iter->second->window_size; i++)
        {
            //printf("its me?   ");
            count = sock_iter->second->sending_seq + WINDOW_LEN*(i+1);
            // @namkyu : seq_num을 관리해주는 count. sending_seq는 내가 보내는 중인 가장 작은 seq_num을 관리함. (current와 다름)
            if(count - sock_iter->second->sending_seq > sock_iter->second->write_offset)
                count = sock_iter->second->write_offset + sock_iter->second->sending_seq;

            if(count - sock_iter->second->sending_seq == sock_iter->second->write_offset){
                //printf("OUT OF MEMORY!!!!!!!!!!!!!!!!!!!!!\n");
                break;
            }   // @namkyu : out of memory.

            sock_iter->second->processing++;    // @namkyu : window 하나가 확정 되었으니 processing을 늘려줌.

            sock_iter->second->window_buffer[i] = count;

            seq_num[0] = htonl(sock_iter->second->sending_seq + WINDOW_LEN*i);
            ack_num[0] = htonl(sock_iter->second->current_ack);
            off_reserve[0] = 0x50;

            window[0] = htons(51200);
            urgent[0] = 0;

            TCPFlags[0] = ACK;

            //printf("ites me???   ");

            uint8_t temp[20 + (count - sock_iter->second->sending_seq - WINDOW_LEN*i)];   // it will be header
            memset(temp, 0, sizeof(temp));
            // @namkyu : 이제 temp를 동적인 정적으로 할당하여 MakePacket에서 Data 까지 쳐넣을 겁니다.
            memcpy(&temp[0], src_port, 2);
            memcpy(&temp[2], dest_port, 2);
            memcpy(&temp[4], seq_num, 4);
            memcpy(&temp[8], ack_num, 4);
            memcpy(&temp[12], off_reserve, 1);
            memcpy(&temp[13], TCPFlags, 1);
            memcpy(&temp[14], window, 2);
            memcpy(&temp[18], urgent, 2);
            memcpy(&temp[20], sock_iter->second->write_buffer + (WINDOW_LEN * i), (count - sock_iter->second->sending_seq - WINDOW_LEN*i));
            // @namkyu : 설호
            //printf("istset me??   ");

            Packet* packet = allocatePacket(54 + (count - sock_iter->second->sending_seq - WINDOW_LEN*i));

            this->makePacket(packet, temp, src_ip, dest_ip, 20 + (count - sock_iter->second->sending_seq - WINDOW_LEN*i));
            // @namkyu : makePacket 함수가 변화가 있었음. 아래에 설명함.
            //printf("ieteisifse mee??????   ");

            this->sendPacket("IPv4", packet);

            if(count > sock_iter->second->current_seq)
            {   // @namkyu : 시퀀스 변화줌. current_seq = 보낸 가장 큰 seq_num. sending_seq와 다름.
                sock_iter->second->current_seq = count;
            }

            //printf("count : %d, dd : %d win : %d = %d\n", count, sock_iter->second->sending_seq, WINDOW_LEN*i, (count - sock_iter->second->sending_seq - WINDOW_LEN*i));

        }

        //printf("WINDOW_SIZE DIE : %d\n", sock_iter->second->processing);
        sock_iter->second->send_complete = true;
        // @namkyu : processing window가 중간에 끝나면 window_size를 줄여줍니다. stimo형이 할 필요 없댔는데, 일단 놔둠.
        sock_iter->second->window_size = sock_iter->second->processing;

    }

    void TCPAssignment::syscall_connect(UUID syscallUUID, int pid, int sockfd, struct sockaddr* servaddr, socklen_t addrlen)
    {
        struct sockaddr_in src_addr;
        struct sockaddr_in dst_addr;

        uint32_t src_ip[4];
        uint32_t dest_ip[4];

        /*TCP Header*/
        uint16_t src_port[2];
        uint16_t dest_port[2];
        uint8_t TCPFlags[1];
        uint32_t seq_num[4];
        uint32_t ack_num[4];

        uint8_t off_reserve[1];
        uint16_t window[2];
        uint16_t urgent[2];

        auto sockNow = sockMap.find(std::make_pair(pid, sockfd));

        if (sockNow == sockMap.end())
        {
            //printf("ther is no sockfd to connect\n");
            this->returnSystemCall(syscallUUID, -1);
            return;
        }

        memset(&src_addr, 0, sizeof(src_addr));
        memset(&dst_addr, 0, sizeof(dst_addr));
        memset(seq_num, 0, sizeof(seq_num));
        memset(ack_num, 0, sizeof(ack_num));

        if (sockNow->second->isBinded)
        {
            src_ip[0] = sockNow->second->sin_addr.s_addr;
            src_port[0] = sockNow->second->sin_port;
        }
        else
        {
            uint8_t first_input[4];
            uint8_t bind_src_ip[4];
            uint32_t temp_dest_ip = ((struct sockaddr_in*)servaddr)->sin_addr.s_addr;
            memcpy(first_input, &temp_dest_ip, 4);

            sockNow->second->sin_port = this->getHost()->getRoutingTable(first_input);
            this->getHost()->getIPAddr(bind_src_ip, sockNow->second->sin_port);
            memcpy(&(sockNow->second->sin_addr.s_addr), bind_src_ip, 4);
            //sockNow->second->sin_addr.s_addr = bind_src_ip[0];

            src_ip[0] = sockNow->second->sin_addr.s_addr;
            src_port[0] = sockNow->second->sin_port;
            sockNow->second->isBinded = true;

            sockNow->second->sa_family = AF_INET;
        }

        dest_ip[0] = ((struct sockaddr_in*)servaddr)->sin_addr.s_addr;
        dest_port[0] = ((struct sockaddr_in*)servaddr)->sin_port;

        sockNow->second->dst_sin_addr.s_addr = dest_ip[0];
        sockNow->second->dst_sin_port = dest_port[0];
        sockNow->second->dst_sa_family = ((struct sockaddr_in*)servaddr)->sin_family;

        sockNow->second->isBinded = true;

        seq_num[0] = htonl(TEMP_SEQ);
        ack_num[0] = 0;
        off_reserve[0] = 0x50;

        window[0] = htons(51200);
        urgent[0] = 0;

        TCPFlags[0] = SYN;

        uint8_t temp[20];	// it will be header
        memset(temp, 0, sizeof(temp));

        memcpy(&temp[0], src_port, 2);
        memcpy(&temp[2], dest_port, 2);
        memcpy(&temp[4], seq_num, 4);
        memcpy(&temp[8], ack_num, 4);
        memcpy(&temp[12], off_reserve, 1);
        memcpy(&temp[13], TCPFlags, 1);
        memcpy(&temp[14], window, 2);
        memcpy(&temp[18], urgent, 2);

        Packet* packet = allocatePacket(54);

        this->makePacket(packet, temp, src_ip, dest_ip, 20);

        this->sendPacket("IPv4", packet);

        sockNow->second->state = SYN_SENT;
        sockNow->second->seq_sent = ntohl(seq_num[0]);
        sockNow->second->sock_UUID = syscallUUID;

        sockNow->second->syn = false;
        sockNow->second->ack = false;
        sockNow->second->now_ack = ntohl(ack_num[0]);
        sockNow->second->now_seq = ntohl(seq_num[0]+1);

        sockNow->second->current_seq = ntohl(seq_num[0]);


        char str[INET_ADDRSTRLEN];

        inet_ntop(AF_INET, &(sockNow->second->sin_addr), str, INET_ADDRSTRLEN);
        //printf("success connect : %d fam :  %d port :  %d add : %s\n", sockfd, sockNow->second->sa_family, sockNow->second->sin_port, str);

        inet_ntop(AF_INET, &(sockNow->second->dst_sin_addr), str, INET_ADDRSTRLEN);
        //printf("success connect : %d fam :  %d port :  %d add : %s\n", sockfd, sockNow->second->dst_sa_family, sockNow->second->dst_sin_port, str);


    }

    void TCPAssignment::syscall_listen(UUID syscallUUID, int pid, int sockfd, int backlog)
    {
        auto nowSock = sockMap.find(std::make_pair(pid, sockfd));

        if (nowSock == sockMap.end())
        {
            this->returnSystemCall(syscallUUID, -1);
            return;
        }

        if ((nowSock->second->state != SOCK_CLOSED && nowSock->second->state != SOCK_LISTEN))
        {
            // If not closed, also listen. it's state fault.
            this->returnSystemCall(syscallUUID, -1);
            return;
        }

        //printf("listen fd : %d , backlog : %d\n", sockfd, backlog);

        if (!(nowSock->second->isBinded))
        {
            // it must be binded
            this->returnSystemCall(syscallUUID, -1);
            return;
        }

        nowSock->second->backlog = backlog;	// Set Backlog.
        nowSock->second->backlog_use = 0;	// set backlog_use is 0

        if (nowSock->second->state == SOCK_CLOSED) {

            nowSock->second->state = SOCK_LISTEN;

        }

        nowSock->second->listen_fd = -1;

        this->returnSystemCall(syscallUUID, 0);	// Success listen.
    }

    void TCPAssignment::syscall_accept(UUID syscallUUID, int pid, int sockfd, struct sockaddr* client_addr, socklen_t* addrlen)
    {
        auto nowSock = sockMap.find(std::make_pair(pid, sockfd));	// find LISTEN socket.

                                                                    //printf("accept pid : %d fd : %d , back : %d use : %d\n", pid, sockfd, nowSock->second->backlog, nowSock->second->backlog_use);

        if (nowSock->second->state != SOCK_LISTEN)	// LISTEN!!!
        {
            this->returnSystemCall(syscallUUID, -1);
            return;
        }

        auto iterSock = sockMap.begin();

        for (iterSock = sockMap.begin(); iterSock != sockMap.end(); iterSock++)
        {
            if ((iterSock->second->pid == nowSock->second->pid) 
            && (iterSock->second->listen_fd == nowSock->second->socket_fd) /*&& (iterSock->second->state == ESTABLISHED)*/)
            {
                break;
            }
        }

        if (iterSock != sockMap.end())	// got a ESTABLISHED client_socket
        {
            iterSock->second->listen_fd = -1;
            ((struct sockaddr_in*)client_addr)->sin_family = iterSock->second->dst_sa_family;
            ((struct sockaddr_in*)client_addr)->sin_addr = iterSock->second->dst_sin_addr;
            ((struct sockaddr_in*)client_addr)->sin_port = iterSock->second->dst_sin_port;

            returnSystemCall(syscallUUID, iterSock->second->socket_fd);
        }
        else
        {
            ((struct sockaddr_in*)client_addr)->sin_family = AF_INET;
            nowSock->second->backlog_use++;
            nowSock->second->accept_wait_UUID = syscallUUID;
            nowSock->second->accept_timer_UUID = this->addTimer(((void*)nowSock->second), 2000000000);

        }

    }

    void TCPAssignment::syscall_bind(UUID syscallUUID, int pid, int sockfd, const struct sockaddr* servaddr, socklen_t addrlen)
    {
        //printf("bind1\n");

        auto sock_iter = sockMap.find(std::make_pair(pid, sockfd));

        if (sock_iter == sockMap.end())
        {
            //printf("NOT EXIST SOCKET FD\n");
            this->returnSystemCall(syscallUUID, -1);
            return;
        }

        char str[INET_ADDRSTRLEN];

        inet_ntop(AF_INET, &(((struct sockaddr_in*)&servaddr)->sin_addr), str, INET_ADDRSTRLEN);
        //printf("%d pid, %d bind to  : %s - %d\n", pid,sockfd, str, ((struct sockaddr_in*)&servaddr)->sin_port);

        if (sock_iter->second->isBinded)
        {
            //printf("already binded %d on syscal %d\n", sockfd,sockMap.size());
            for(auto iter = sockMap.begin(); iter != sockMap.end(); iter++)
            {
                //printf("key %d,%d\n",iter->first.first,iter->first.second);
            }
            this->returnSystemCall(syscallUUID, -1);
            return;
        }

        for(auto iter = sockMap.begin(); iter != sockMap.end(); iter++)
        {
            //printf("key %d,%d\n",iter->first.first,iter->first.second);
        }

        do_bind(pid, sockfd, servaddr, addrlen);

        if (!sock_iter->second->isBinded)
        {
            this->returnSystemCall(syscallUUID, -1);
            return;
        }

        sock_iter->second->listen_fd = -1;

        //printf("bind : %d fam :  %d port :  %d \n", sockfd, sock_iter->second->sa_family, sock_iter->second->sin_port);


        this->returnSystemCall(syscallUUID, 0);
    }

    void TCPAssignment::syscall_getsockname(UUID syscallUUID, int pid, int sockfd, struct sockaddr* addr, socklen_t* addrlen)
    {
        //printf("sock MAp len : %d\n", sockMap.size());

        auto sock_iter = sockMap.find(std::make_pair(pid, sockfd));
        if (sock_iter == sockMap.end())
        {
            //printf("%s\n", "hi");
            this->returnSystemCall(syscallUUID, -1);
            return;
        }

        ((struct sockaddr_in*)addr)->sin_family = sock_iter->second->sa_family;
        ((struct sockaddr_in*)addr)->sin_addr = sock_iter->second->sin_addr;
        ((struct sockaddr_in*)addr)->sin_port = sock_iter->second->sin_port;

        //printf("pid : %d, sockfd : %d\n", pid, sockfd);
        this->returnSystemCall(syscallUUID, 0);
    }

    void TCPAssignment::syscall_getpeername(UUID syscallUUID, int pid, int sockfd, struct sockaddr* addr, socklen_t* addrlen)
    {
        //printf("getpeer : %d\n", sockfd);
        auto sock_iter = sockMap.find(std::make_pair(pid, sockfd));
        if (sock_iter == sockMap.end())
        {
            //printf("failed to get fd\n");
            this->returnSystemCall(syscallUUID, -1);
            return;
        }

        ((struct sockaddr_in*)addr)->sin_family = sock_iter->second->dst_sa_family;
        ((struct sockaddr_in*)addr)->sin_addr = sock_iter->second->dst_sin_addr;
        ((struct sockaddr_in*)addr)->sin_port = sock_iter->second->dst_sin_port;

        this->returnSystemCall(syscallUUID, 0);
    }

    void TCPAssignment::systemCallback(UUID syscallUUID, int pid, const SystemCallParameter& param)
    {
        //printf("UUID %d %d\n", syscallUUID, param.syscallNumber);
        switch (param.syscallNumber)
        {
        case SOCKET:
            this->syscall_socket(syscallUUID, pid, param.param1_int, param.param2_int, param.param3_int);
            break;
        case CLOSE:
            this->syscall_close(syscallUUID, pid, param.param1_int);
            break;
        case READ:
            this->syscall_read(syscallUUID, pid, param.param1_int, param.param2_ptr, param.param3_int);
            break;
        case WRITE:
            this->syscall_write(syscallUUID, pid, param.param1_int, param.param2_ptr, param.param3_int);
            break;
        case CONNECT:
            this->syscall_connect(syscallUUID, pid, param.param1_int,
                static_cast<struct sockaddr*>(param.param2_ptr), (socklen_t)param.param3_int);
            break;
        case LISTEN:
            this->syscall_listen(syscallUUID, pid, param.param1_int, param.param2_int);
            break;
        case ACCEPT:
            this->syscall_accept(syscallUUID, pid, param.param1_int,
                static_cast<struct sockaddr*>(param.param2_ptr),
                static_cast<socklen_t*>(param.param3_ptr));
            break;
        case BIND:
            this->syscall_bind(syscallUUID, pid, param.param1_int,
                static_cast<struct sockaddr *>(param.param2_ptr),
                (socklen_t)param.param3_int);
            break;
        case GETSOCKNAME:
            this->syscall_getsockname(syscallUUID, pid, param.param1_int,
                static_cast<struct sockaddr *>(param.param2_ptr),
                static_cast<socklen_t*>(param.param3_ptr));
            break;
        case GETPEERNAME:
            this->syscall_getpeername(syscallUUID, pid, param.param1_int,
                static_cast<struct sockaddr *>(param.param2_ptr),
                static_cast<socklen_t*>(param.param3_ptr));
            break;
        default:
            assert(0);
        }
    }

    //void make_tcp_header()

    Packet* TCPAssignment::makePacket(Packet* myPacket, uint8_t *temp, uint32_t* src_ip, uint32_t* dst_ip, int length)
    {   // @namkyu : makePacket 친구가 변신했음. 이제 length를 받고 data를 넣을 수 있어짐. 물론 checksum도.
        //printf("make pacekt!!!!!!\n");

        myPacket->writeData(26, src_ip, 4);	// src_ip on
        myPacket->writeData(30, dst_ip, 4);	// dest_ip on

        uint16_t checksum[2];

        myPacket->writeData(34, &temp[0], 2);
        myPacket->writeData(36, &temp[2], 2);
        myPacket->writeData(38, &temp[4], 4);
        myPacket->writeData(42, &temp[8], 4);
        myPacket->writeData(46, &temp[12], 1);
        myPacket->writeData(47, &temp[13], 1);
        myPacket->writeData(48, &temp[14], 2);
        memset(checksum, 0, sizeof(checksum));
        myPacket->writeData(52, &temp[18], 2);


        checksum[0] = htons(~NetworkUtil::tcp_sum(src_ip[0], dst_ip[0], temp, (size_t)length));

        myPacket->writeData(50, checksum, 2);
        // @namkyu : 자세한 설명은 생략한다.
        if(length > 20){
            myPacket->writeData(54, &temp[20], length - 20);
        }

        return myPacket;
    }


    void TCPAssignment::packetArrived(std::string fromModule, Packet* packet)
    {
        //TcpHeader tcp_hdr;
        pseudoHeader hdr_rcv;

        void* header[26];
        struct sockaddr_in src_addr;
        struct sockaddr_in dst_addr;
        uint32_t src_ip[4];
        uint32_t dest_ip[4];

        /*TCP Header*/
        uint16_t src_port[2];
        uint16_t dest_port[2];
        uint8_t TCPFlags[1];
        uint32_t seq_num[4];
        uint32_t ack_num[4];

        uint8_t off_reserve[1];
        uint16_t window[2];
        uint16_t checksum[2];
        uint16_t urgent[2];

        uint32_t seq;
        uint32_t ack;
        uint8_t new_flag = 0;

        Packet* myPacket = this->allocatePacket(54);	// clone packet.

        memset(&src_addr, 0, sizeof(src_addr));
        memset(&dst_addr, 0, sizeof(dst_addr));
        memset(seq_num, 0, sizeof(seq_num));
        memset(ack_num, 0, sizeof(ack_num));


        packet->readData(0, header, 26);
        myPacket->writeData(0, header, 26);

        /*read on packet data */
        packet->readData(26, dest_ip, 4);
        packet->readData(30, src_ip, 4);
        packet->readData(34, dest_port, 2);
        packet->readData(36, src_port, 2);
        packet->readData(38, seq_num, 4);
        packet->readData(42, ack_num, 4);
        packet->readData(46, off_reserve, 1);
        packet->readData(48, window, 2);
        packet->readData(50, checksum, 2);
        packet->readData(52, urgent, 2);

        src_addr.sin_addr.s_addr = src_ip[0];
        dst_addr.sin_addr.s_addr = dest_ip[0];
        src_addr.sin_port = src_port[0];
        dst_addr.sin_port = dest_port[0];



        src_addr.sin_family = AF_INET;
        src_addr.sin_addr.s_addr = src_ip[0];
        src_addr.sin_port = src_port[0];

        dst_addr.sin_family = AF_INET;
        dst_addr.sin_addr.s_addr = dest_ip[0];
        dst_addr.sin_port = dest_port[0];

        hdr_rcv.seq_num = ntohl(seq_num[0]);
        hdr_rcv.ack_num = ntohl(ack_num[0]);
        hdr_rcv.src_port = ntohs(src_port[0]);
        hdr_rcv.dst_port = ntohs(dest_port[0]);
        hdr_rcv.window_size = ntohs(window[0]);
        hdr_rcv.checksum = ntohs(checksum[0]);
        hdr_rcv.urg_ptr = ntohs(urgent[0]);

        packet->readData(47, TCPFlags, 1);	// read TCPFlags

                                            /* set flags */
        hdr_rcv.fin = (TCPFlags[0] & FIN) == 0 ? 0 : 1;
        hdr_rcv.syn = (TCPFlags[0] & SYN) == 0 ? 0 : 1;
        hdr_rcv.rst = (TCPFlags[0] & RST) == 0 ? 0 : 1;
        hdr_rcv.psh = (TCPFlags[0] & PSH) == 0 ? 0 : 1;
        hdr_rcv.ack = (TCPFlags[0] & ACK) == 0 ? 0 : 1;
        hdr_rcv.urg = (TCPFlags[0] & URG) == 0 ? 0 : 1;

        /*
        tcp_hdr.src_port = hdr->src_port;
        tcp_hdr.dest_port = hdr->dst_port;
        tcp_hdr.seq_num = hdr->seq_num;
        tcp_hdr.ack_num = hdr->ack_num;
        tcp_hdr.offset = ntohs(off_reserve[0]);
        tcp_hdr.flag = ntohs(TCPFlags[0]);
        tcp_hdr.window_size = ntohs(window[0]);
        tcp_hdr.checksum = 0;
        tcp_hdr.urg_ptr = ntohs(urgent[0]);
        */

        //printf("ack : %01d syn : %01d fin : %d\n", hdr_rcv.ack, hdr_rcv.syn, hdr_rcv.fin);


        auto sockNow = find_TCP_socket(dst_addr, src_addr, hdr_rcv.syn);

        if (sockNow == sockMap.end()) {
            //printf("we can't find sock to packet\n");
            return;
        }

        //printf("find sock is : %d\n", sockNow->first);

        //printf("ack : %01d syn : %01d fin : %d fd : %d\n", hdr_rcv.ack, hdr_rcv.syn, hdr_rcv.fin, sockNow->first);

        //printf("ack : %01d syn : %01d fin : %d fd : %d, state : %d\n", hdr_rcv.ack, hdr_rcv.syn, hdr_rcv.fin, sockNow->second->socket_fd, sockNow->second->state);

        if (packet->getSize() - 54 > 0)
        {
            if(sockNow->second->read_offset + packet->getSize() - 54 <= BUFFER_MAX)
            {
                memcpy(sockNow->second->read_buffer + sockNow->second->read_offset, packet + 54, packet->getSize() - 54);
                sockNow->second->read_offset += packet->getSize() - 54;
            }
            // @namkyu : Established 상태에서만 ACK 보내는거 여기로 옮김. Stimo 형이 Close 되어도 ACK 반응이 가능해야 하댔음.
            if ((sockNow->second->read_count > 0) && (packet->getSize() - 54 > 0))
                {
                    if(sockNow->second->read_count > sockNow->second->read_offset)
                        sockNow->second->read_count = sockNow->second->read_offset;

                        memcpy(sockNow->second->read_delayed_buffer, sockNow->second->read_buffer, sockNow->second->read_count);
                        memmove(sockNow->second->read_buffer, sockNow->second->read_buffer + sockNow->second->read_count, sockNow->second->read_offset - sockNow->second->read_count);
                        sockNow->second->read_offset -= sockNow->second->read_count;

                        //printf("unblockcl UUID : %d\n", sockNow->second->readcall_delay_UUID);
                        this->returnSystemCall(sockNow->second->readcall_delay_UUID, sockNow->second->read_count);

                        new_flag = ACK;
                        sockNow->second->current_ack += packet->getSize() - 54;
                        seq = htonl(sockNow->second->current_seq);
                        ack = htonl(sockNow->second->current_ack);
                        memcpy(seq_num, &seq, sizeof(seq));
                        memcpy(ack_num, &ack, sizeof(ack));
                        memcpy(TCPFlags, &new_flag, sizeof(new_flag));

                        sockNow->second->read_count = -1;
                }
        }

        switch (sockNow->second->state)
        {
        case SOCK_LISTEN:
            //printf("불행하다! 리슨이야!\n");
            if (hdr_rcv.syn)
            {
                auto iter_sock = sockMap.begin();

                for (iter_sock = sockMap.begin(); iter_sock != sockMap.end(); iter_sock++)
                {
                    if ((iter_sock->second->pid == sockNow->second->pid) && (iter_sock->second->listen_fd == sockNow->second->socket_fd) && (iter_sock->second->state != ESTABLISHED))
                        break;
                }

                if (sockNow->second->backlog > 0)
                {
                    int fd = createFileDescriptor(sockNow->second->pid);

                    SocketInfo* newSock = new SocketInfo();

                    sockMap.insert(SockMap::value_type(std::make_pair(sockNow->second->pid, fd), newSock));

                    newSock->pid = sockNow->second->pid;

                    newSock->sin_port = src_addr.sin_port;
                    newSock->sin_addr = src_addr.sin_addr;
                    newSock->sa_family = AF_INET;

                    newSock->dst_sin_port = dst_addr.sin_port;
                    newSock->dst_sin_addr = dst_addr.sin_addr;
                    newSock->dst_sa_family = AF_INET;

                    //@ryang offset 초기화
        			newSock->read_offset = 0;
        			newSock->write_offset = 0;
                    newSock->readcall_delay_UUID = -1;
                    newSock->sending_seq = TEMP_SEQ+1;

                    newSock->socket_fd = fd;


                    newSock->isBinded = true;

                    newSock->listen_fd = sockNow->second->socket_fd;

                    sockNow->second->backlog--;

                    //sockNow->second->state = SYN_RCVD;
                    newSock->state = SYN_RCVD;

                    //printf("new socket %d %d\n",newSock->pid,newSock->socket_fd);
					
					newSock->current_seq = TEMP_SEQ;
                    newSock->current_ack = ntohl(seq_num[0]) + 1;

                    new_flag = 0x12;

                    seq = htonl(newSock->current_seq);
                    ack = htonl(newSock->current_ack);   

                    memcpy(seq_num, &seq, sizeof(seq));
                    memcpy(ack_num, &ack, sizeof(ack));
                    memcpy(TCPFlags, &new_flag, sizeof(new_flag));


                }
                else
	                return;

            }
            else
            	return;
            break;

        case SYN_RCVD:

            if (hdr_rcv.ack)
            {
                auto iter_sock = sockMap.find(std::make_pair(sockNow->second->pid, sockNow->second->listen_fd));

                sockNow->second->state = ESTABLISHED;

                sockNow->second->current_seq += 1;

                iter_sock->second->backlog++;

                if (iter_sock->second->backlog_use > 0)
                {
                    iter_sock->second->backlog_use--;
                    //printf("output timer %d\n", iter_sock->second->UUIDlist.front());
                    //cancelTimer(iter_sock->second->Timerlist.front());

                    sockNow->second->listen_fd = -1;
                    cancelTimer(iter_sock->second->accept_timer_UUID);
                    returnSystemCall(iter_sock->second->accept_wait_UUID, sockNow->second->socket_fd);
                    iter_sock->second->accept_wait_UUID = -1;
                    /*iter_sock->second->UUIDlist.pop();
                    iter_sock->second->Timerlist.pop();*/
                }

                return;
            }
            else
            	return;
            break;


        case SYN_SENT:
            sockNow->second->syn |= hdr_rcv.syn;
            sockNow->second->ack |= hdr_rcv.ack;

            new_flag = 0;

            if (hdr_rcv.ack)
            	sockNow->second->current_seq += 1;

            if (hdr_rcv.syn)
            {
                sockNow->second->current_ack = ntohl(seq_num[0]) + 1;

                seq = htonl(sockNow->second->current_seq);
                ack = htonl(sockNow->second->current_ack);
                
                new_flag |= ACK;
            }

            if (sockNow->second->syn && sockNow->second->ack)
            {
                sockNow->second->state = ESTABLISHED;
                returnSystemCall(sockNow->second->sock_UUID, 0);
                sockNow->second->sock_UUID = -1;
            }

            if (new_flag == 0)
                return;

            memcpy(seq_num, &seq, sizeof(seq));
            memcpy(ack_num, &ack, sizeof(ack));
            memcpy(TCPFlags, &new_flag, sizeof(new_flag));

            sockNow->second->seq_sent = ntohl(seq);

            break;

        case ESTABLISHED:
            if (hdr_rcv.fin)
            {
	            new_flag = ACK;
	            sockNow->second->current_ack += 1;

	            seq = htonl(sockNow->second->current_seq);
	            ack = htonl(sockNow->second->current_ack);

	            memcpy(seq_num, &seq, sizeof(seq));
	            memcpy(ack_num, &ack, sizeof(ack));
	            memcpy(TCPFlags, &new_flag, sizeof(new_flag));

                sockNow->second->state = CLOSE_WAIT;

                if(sockNow->second->read_count > 0)
                {
                    returnSystemCall(sockNow->second->readcall_delay_UUID, -1);
                }
            }
            else if(hdr_rcv.ack)
            {   // @namkyu : 대망의 write에서 보낸 놈의 ACK 처리 구간임.
                if(packet->getSize() <= 54)   // reponse by data send
                {
                    //printf("when i was dreamped!!! %d %d\n", ntohl(ack_num[0]), sockNow->second->processing);
                    if(sockNow->second->processing >= 0)
                    {   // @namkyu : 우리의 친구 processing이 나왔음.
                        int i;

                        for(i = 0; i < (sockNow->second->processing - sockNow->second->received_packet); i++)
                        {
                            //printf("check : %d - %d\n", sockNow->second->window_buffer[i], hdr_rcv.ack_num);
                            if(sockNow->second->window_buffer[i] == hdr_rcv.ack_num)
                            {   // @namkyu :  원하는 ACK가 왔으면, 해당 buffer에 받았다는 체크를 해줌. 코드가 좀 더러움.
                                sockNow->second->window_buffer[i] *= -1;    // reverse, when it's worked.
                                break;
                            }
                        }

                        //printf("sdfsdfsdf %d %d sdfsdfsdf\n", sockNow->second->window_buffer[0], sockNow->second->window_buffer[1]);

                        while((sockNow->second->processing > sockNow->second->received_packet) && (sockNow->second->window_buffer[0] < 0))
                            {   // @namkyu : 여기서 received_packet이 나오는데, write_buffer에서 제거한 수를 체크하게됨.
                                //printf("후히히☆ %d\n", sockNow->second->window_buffer[0]);
                                memmove(sockNow->second->write_buffer, sockNow->second->write_buffer - sockNow->second->window_buffer[0] - sockNow->second->sending_seq,
                                    sockNow->second->write_offset + sockNow->second->window_buffer[0] + sockNow->second->sending_seq);
                                // @namkyu :  흔한 memmove
                                sockNow->second->write_offset += sockNow->second->window_buffer[0] + sockNow->second->sending_seq;
                                sockNow->second->sending_seq = -sockNow->second->window_buffer[0];
                                // @namkyu : sending_seq는 증가함. 보내는 중인 가장 적은 seq_num이 변했기 때문.
                                for(i = 1; i < sockNow->second->processing; i++){
                                    sockNow->second->window_buffer[i-1] = sockNow->second->window_buffer[i];
                                    //printf("---change value : %d\n", sockNow->second->window_buffer[i]);
                                }   // @namkyu : 이유는 모르겠는데, 여기는 memmove를 쓰면 안됨.
                                sockNow->second->received_packet++;
                                // @namkyu : proccesing buffer 에서 전송 완료된 packet의 수를 뜻함.
                        }

                        //printf("processing : %d\n", sockNow->second->processing);

                        if((sockNow->second->processing == sockNow->second->received_packet))
                        {   // @namkyu : 보낸 windowsize와 받은 windowsize가 드디어 같게됨.
                            //printf("이겼다 또 이겼다\n");
                            sockNow->second->window_size *= 2;  // @namkyu : stimo형이 + 1 하랬지만, 그렇게 해도 안되서 이렇게함.
                            //sockNow->second->window_size++;
                            sockNow->second->processing = -1;   // @namkyu : processing은 다음을 기약함.

                            if(sockNow->second->write_offset < BUFFER_MAX && (sockNow->second->write_count > 0))
                            {   // @namkyu : block 된 write가 있는가 확인함.

                                if(sockNow->second->write_count + sockNow->second->write_offset > BUFFER_MAX)
                                    sockNow->second->write_count = BUFFER_MAX - sockNow->second->write_offset;

                                memcpy(sockNow->second->write_buffer + sockNow->second->write_offset, sockNow->second->write_delayed_buffer, sockNow->second->write_count);
                                sockNow->second->write_offset += sockNow->second->write_count;
                                // @namkyu : 있으면 평상적인 방법으로 write 하고 return 함.
                                //printf("exception write : %d %d \n", sockNow->second->write_count, sockNow->second->write_offset);

                                this->returnSystemCall(sockNow->second->writecall_delay_UUID, sockNow->second->write_count);

                                sockNow->second->write_count = 0;
                                sockNow->second->writecall_delay_UUID = -1;
                                sockNow->second->write_delayed_buffer = NULL;
                            }

                            if(sockNow->second->write_offset > 0)
                            {   // @namkyu : window size 만큼 ACK를 받고 나서도 write_buffer가 차있으면 다시 전송을 시작함.
                                //printf("Now offset if : %d\n", sockNow->second->write_offset);
                                send_writebuffer(sockNow->second->pid, sockNow->second->socket_fd);
                            } else{ // @namkyu : 더이상 buffer가 쌓여있지 않으면 sending 상태를 false로 하여줌.
                                //printf("We NEEEEED to Recover this\n");
                                sockNow->second->packet_sending = false;
                            }
                        }
                    }
                    return;
                }
            }
            else
            	return;
            break;
        case LAST_ACK:

            if (hdr_rcv.ack)
            {

              sockMap.erase(std::make_pair(sockNow->second->pid, sockNow->second->socket_fd));
              sockNow->second->state = SOCK_CLOSED;
              return;
            }
            else if(hdr_rcv.fin)
            {
              new_flag = ACK;
              seq = htonl(sockNow->second->current_seq);
              ack = htonl(sockNow->second->current_ack);
              memcpy(seq_num, &seq, sizeof(seq));
              memcpy(ack_num, &ack, sizeof(ack));
              memcpy(TCPFlags, &new_flag, sizeof(new_flag));
            }
            else
            	return;
            break;

        case FIN_WAIT_1:
            if (hdr_rcv.ack)
            {
            	sockNow->second->current_seq += 1;
            	returnSystemCall(sockNow->second->sock_UUID, 0);
                sockNow->second->state = FIN_WAIT_2;
                return;
            }
            else if(hdr_rcv.fin)
            {
              new_flag = ACK;

              sockNow->second->current_seq += 1;
              sockNow->second->current_ack += 1;
              seq = htonl(sockNow->second->current_seq);
              ack = htonl(sockNow->second->current_ack);
              memcpy(seq_num, &seq, sizeof(seq));
              memcpy(ack_num, &ack, sizeof(ack));
              memcpy(TCPFlags, &new_flag, sizeof(new_flag));

              sockNow->second->state = FIN_ACK_WAIT;

            }
            else
            	return;
            break;

        case FIN_WAIT_2:
            if (hdr_rcv.fin)
            {
                new_flag = ACK;

                sockNow->second->current_ack += 1;

                seq_num[0] = htonl(sockNow->second->current_seq);
                ack_num[0] = htonl(sockNow->second->current_ack);
               
                memcpy(TCPFlags, &new_flag, sizeof(new_flag));

                sockNow->second->state = TIMED_WAIT;

                //@ryang 아무리 sockMap을 남겨도 계속 에러나길래 TimedWait 친구만 따로 관리
                TimedWaitsockMap.insert(SockMap::value_type(std::make_pair(sockNow->second->pid, sockNow->second->socket_fd), sockNow->second));

                sockMap.erase(std::make_pair(sockNow->second->pid, sockNow->second->socket_fd));

                //printf("Map Size %d\n", sockMap.size());
                this->addTimer(((void*)(sockNow->second)), 2400000000000);

                this->returnSystemCall(sockNow->second->sock_UUID, 0);
            }
            else
            	return;
            break;
         case TIMED_WAIT:
		         if(hdr_rcv.fin)
	            {
	            	new_flag = ACK;
	              seq = htonl(sockNow->second->current_seq);
	              ack = htonl(sockNow->second->current_ack);
	              memcpy(seq_num, &seq, sizeof(seq));
	              memcpy(ack_num, &ack, sizeof(ack));
	              memcpy(TCPFlags, &new_flag, sizeof(new_flag));
	            }
	            else
	            	return;
         			break;
        case FIN_ACK_WAIT:
            if (hdr_rcv.ack)
            {
                sockNow->second->state = TIMED_WAIT;
                //@ryang 아무리 sockMap을 남겨도 계속 에러나길래 TimedWait 친구만 따로 관리
                TimedWaitsockMap.insert(SockMap::value_type(std::make_pair(sockNow->second->pid, sockNow->second->socket_fd), sockNow->second));
                sockMap.erase(std::make_pair(sockNow->second->pid, sockNow->second->socket_fd));
                this->addTimer(((void*)(sockNow->second)), 2400000000000);
                this->returnSystemCall(sockNow->second->sock_UUID, 0);
                return;
            }
            else if(hdr_rcv.fin)
            {
              new_flag = ACK;
              seq = htonl(sockNow->second->current_seq);
	          ack = htonl(sockNow->second->current_ack);
              memcpy(seq_num, &seq, sizeof(seq));
              memcpy(ack_num, &ack, sizeof(ack));
              memcpy(TCPFlags, &new_flag, sizeof(new_flag));

              sockNow->second->state = FIN_ACK_WAIT;

            }
            break;
        default:
        	return;
            break;
        }

        this->freePacket(packet);

        uint8_t temp[20];	// it will be header
        memset(temp, 0, sizeof(temp));

        memcpy(&temp[0], src_port, 2);
        memcpy(&temp[2], dest_port, 2);
        memcpy(&temp[4], seq_num, 4);
        memcpy(&temp[8], ack_num, 4);
        memcpy(&temp[12], off_reserve, 1);
        memcpy(&temp[13], TCPFlags, 1);
        memcpy(&temp[14], window, 2);
        memcpy(&temp[18], urgent, 2);

        this->makePacket(myPacket, temp, src_ip, dest_ip, 20);

        this->sendPacket("IPv4", myPacket);

    }

    void TCPAssignment::timerCallback(void* payload)
    {

        SocketInfo* sockNow = (SocketInfo*)payload;
        if (sockNow->state == TIMED_WAIT)
        {
        	TimedWaitsockMap.erase(std::make_pair(sockNow->pid, sockNow->socket_fd));
            this->removeFileDescriptor(sockNow->pid, sockNow->socket_fd);
            delete sockNow;

            return;
        }
        else
        {
            sockNow->backlog_use--;
            returnSystemCall(sockNow->accept_wait_UUID, -1);
        }
    }


}
